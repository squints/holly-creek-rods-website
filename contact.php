<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="style.css"> 

<title></title>
</head>
<body>

<div id="main">
<div id="top"><a href="index.html"><img id="logo" src="logo.png"></a><span id="logotxt">Holly Creek Rod & Reel</span></div>
<div id="menu">
<div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
         <li><a href="index.html">Home</a></li>
          <li><a href="gallery.html">Gallery</a></li>
           <li class="selected"><a href="contact.html">Contact Us</a></li>
        </ul>
</div></div>

<div id="maintxt">
<h1>Contact us</h1>

<?php
$action=$_REQUEST['action'];
if ($action=="")    /* display the contact form */
    {
    ?>
    <form  action="" method="POST" enctype="multipart/form-data">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr><td>Name:</td><td><input type="edit" name="name" value="John Doe" size="20"></td></tr>
<tr><td>Email:</td><td><input type="edit" name="email" value="you@email.com" size="20"></td></tr>
<tr><td>Phone:</td><td>+<input type="edit" name="phone" value="18005551234" size="11"></td></tr>
<tr><td>Tell us what you need:</td><td><input type="edit" name="message" value="" size="20"></td></tr>
<tr><td colspan=2><input type="submit" value="Send email"/></td></tr>
</table></form>
    <?php
    } 
else                /* send the submitted data */
    {
    $name=$_REQUEST['name'];
    $email=$_REQUEST['email'];
    $message=$_REQUEST['message'];
    if (($name=="")||($email=="")||($message=="")||($phone==""))
        {
        echo "All fields are required, please fill <a href=\"\">the form</a> again.";
        }
    else{        
        $body = "You have been contacted by: ".$POST["name"]." through the website. 
			You may reach him at: (phone) ".$POST["phone"]." or (email) ".$POST["email"].".\r\nSome info: \"".$POST["message"]."\"\r\n\r\n==THIS IS AN AUTOMATED MESSAGE. DO NOT REPLY.==";
 			

			mail("drew@hollycreekrods.com","Website Contact [auto]",$body,"hollycreekrods.com");
			echo "Your message was sent! We will get back to you soon.";
        }
    }  
?> 


</div></div>

</body>
<div id="footer">Site created by <a href="mccarsky.info">Mitchell H. McCarsky</a></div>
</html>